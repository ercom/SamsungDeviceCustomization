SamsungDeviceCustomization
==============

SamsungDeviceCustomization is a helper developed to ease customize samsung device by using both Android and Samsung knox APIS. 

Samsung knox APIS require a valid ELM license (see https://seap.samsung.com/license-keys/about).

Supported customizations
------------
- Wallpapers: both system and lockscreen
- Applications shortcuts: added to all home launchers
- Native browser bookmarks

customizations steps
------------
- Knox ELM License: in app/src/main/java/com/application/samsungcustomization/KnoxLicenseSamsung.java, replace ELM\_LICENSE value by any valid elm license

- Wallpapers: replace both wallpaper\_system.jpg and wallpaper\_lock.jpg, located in app/src/main/res/drawable by any valid images. wallpaper\_system.jpg and wallpaper\_lock.jpg names must be keeped !

- Applications shortcuts: in app/src/main/java/com/application/samsungcustomization/KnoxShortcutCustomization.java, put any valid package name (related application must be installed) in KNOX\_SHORTCUTS\_LIST

- Native browser bookmarks: in app/src/main/java/com/application/samsungcustomization/KnoxNativeBrowserCustomization.java, put any valid pair of URL and bookmark name in KNOX\_BOOKMARKS\_LIST

current limitations
------------
- Applications shortcuts are cumulative: if you relaunch application, new shortcuts are created without removing old shortcuts
