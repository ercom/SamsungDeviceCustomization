package com.application.samsungcustomization;

import android.annotation.SuppressLint;
import android.app.enterprise.EnterpriseDeviceManager;
import android.content.Context;
import android.app.enterprise.ApplicationPolicy;
import android.os.Handler;
import android.os.Looper;

import com.application.samsungcustomization.utils.CustomizationStatus;
import com.application.samsungcustomization.utils.CustomizationAsync;

import java.util.Arrays;
import java.util.LinkedList;

import static com.application.samsungcustomization.utils.CustomizationStatus.CUSTOMIZATION_OK;

/**
 * Created by aci on 27/03/18.
 * There is an issue when successive calls to addHomeShortcut are made (second shortcut erase the first one !).
 * A "badly" (using proper delayed message) sleep was added between calls.
 * This is why this class implements Async approach instead of sync
 */

public class KnoxShortcutCustomization extends CustomizationAsync {

    private static final LinkedList<String> KNOX_SHORTCUTS_LIST =
            new LinkedList<String>(Arrays.asList(
                    "com.ercom.cryptosmart",
                    "com.sec.android.app.sbrowser"));

    private static final int DELAY_BETWEEN_CALLS_MS = 1000;

    @Override
    protected void customizeSpecific(Context context, AsynchronousResponse asynchronousResponse) {
        @SuppressLint("WrongConstant") EnterpriseDeviceManager edm = (EnterpriseDeviceManager) context.getSystemService(EnterpriseDeviceManager.ENTERPRISE_POLICY_SERVICE);
        LinkedList<String> remaining_shortcuts_to_add = new LinkedList<String>(KNOX_SHORTCUTS_LIST);
        handleRemainingShortcuts(edm.getApplicationPolicy(), remaining_shortcuts_to_add, asynchronousResponse);
    }

    protected void handleRemainingShortcuts(final ApplicationPolicy applicationPolicy, final LinkedList<String> remaining_shortcuts_to_add, final AsynchronousResponse asynchronousResponse) {

        if (remaining_shortcuts_to_add.isEmpty()) {
            if (asynchronousResponse != null) {
                asynchronousResponse.onResponse(new CustomizationStatus(false, "(" + getCustomizationName() + "): " + CUSTOMIZATION_OK));
            }
            return;
        }

        String currentPackageName = remaining_shortcuts_to_add.pollFirst();
        CustomizationStatus customizationStatus = customizeShortcut(applicationPolicy, currentPackageName);
        if (customizationStatus.m_isError) {
            if (asynchronousResponse != null) {
                asynchronousResponse.onResponse(new CustomizationStatus(false, "(" + getCustomizationName() + "): " + customizationStatus.m_statusMessage));
            }
            return;
        }

        if (remaining_shortcuts_to_add.isEmpty()) {
            if (asynchronousResponse != null) {
                asynchronousResponse.onResponse(new CustomizationStatus(false, "(" + getCustomizationName() + "): " + CUSTOMIZATION_OK));
            }
            return;
        }

        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {

            @Override
            public void run() {
                handleRemainingShortcuts(applicationPolicy, remaining_shortcuts_to_add, asynchronousResponse);
            }
        }, DELAY_BETWEEN_CALLS_MS);
    }

    @Override
    protected String getCustomizationName() {
        return "KnoxShortcut";
    }

    private CustomizationStatus customizeShortcut(ApplicationPolicy applicationPolicy, String packageName) {
        CustomizationStatus customizationStatus = new CustomizationStatus();
        boolean status = applicationPolicy.addHomeShortcut(packageName, null);
        if (!status) {
            customizationStatus.m_isError = true;
            customizationStatus.m_statusMessage = "Error when knox customize shortcut for package: " + packageName;
        }
        return customizationStatus;
    }

}
