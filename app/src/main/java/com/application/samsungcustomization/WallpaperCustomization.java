package com.application.samsungcustomization;

import android.app.WallpaperManager;
import android.content.Context;

import com.application.samsungcustomization.utils.CustomizationStatus;
import com.application.samsungcustomization.utils.CustomizationSync;

import java.io.IOException;

/**
 * Created by aci on 26/03/18.
 */

public class WallpaperCustomization extends CustomizationSync {

    @Override
    protected CustomizationStatus customizeSpecific(Context context) {
        WallpaperManager wallpaperManager = context.getSystemService(WallpaperManager.class);
        CustomizationStatus customizationStatus = customizeWallpaper(wallpaperManager, R.drawable.wallpaper_system, WallpaperManager.FLAG_SYSTEM);
        customizeWallpaper(wallpaperManager, R.drawable.wallpaper_lock, WallpaperManager.FLAG_LOCK);
        return customizationStatus;
    }

    @Override
    protected String getCustomizationName() {
        return "Wallpaper";
    }

    private CustomizationStatus customizeWallpaper(WallpaperManager wallpaperManager, int resid, int which) {
        CustomizationStatus customizationStatus = new CustomizationStatus();
        try {
            int wallpaperId = wallpaperManager.setResource(resid, which);
            if (wallpaperId == 0) {
                customizationStatus.m_isError = true;
                customizationStatus.m_statusMessage = "Error when customize wallpaper";
            }
        } catch (IOException e) {
            customizationStatus.m_isError = true;
            customizationStatus.m_statusMessage = "IOException when customize wallpaper";
            e.printStackTrace();
        }
        return customizationStatus;
    }
}
