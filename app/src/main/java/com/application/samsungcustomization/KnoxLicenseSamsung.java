package com.application.samsungcustomization;

import android.app.enterprise.license.EnterpriseLicenseManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.application.samsungcustomization.utils.CustomizationAsync;
import com.application.samsungcustomization.utils.CustomizationStatus;

import static com.application.samsungcustomization.utils.CustomizationStatus.CUSTOMIZATION_OK;

public class KnoxLicenseSamsung extends CustomizationAsync {

  final String ELM_LICENSE = "LICENSE_EXAMPLE";

  private static AsynchronousResponse m_asynchronousResponse; /// kuldge
  @Override
  protected String getCustomizationName() {
    return "KnoxLicenseSamsung";
  }

  @Override
  protected void customizeSpecific(final Context context, final AsynchronousResponse asynchronousResponse) {
    m_asynchronousResponse = asynchronousResponse;
    activateNewLicenses(context);
  }

  private void activateNewLicenses(final Context context) {
    EnterpriseLicenseManager elmMgr
            = EnterpriseLicenseManager.getInstance(context);
    if (elmMgr == null) {
      if (m_asynchronousResponse != null) {
        m_asynchronousResponse.onResponse(new CustomizationStatus(false, "(" + getCustomizationName() + "): " + "Error when get EnterpriseLicenseManager"));
        m_asynchronousResponse = null;
      }
      return;
    }

    try {
      elmMgr.activateLicense(ELM_LICENSE,
              context.getPackageName());
    } catch (Exception exception) {
      if (m_asynchronousResponse != null) {
        m_asynchronousResponse.onResponse(new CustomizationStatus(false, "(" + getCustomizationName() + "): " + "Exception when request knox license"));
        m_asynchronousResponse = null;
      }
      exception.printStackTrace();
      return;
    }
  }

  public static class KnoxElmLicenseReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, final Intent intent) {
      int samsungReturnValue = intent.getIntExtra(
              EnterpriseLicenseManager.EXTRA_LICENSE_ERROR_CODE,
              EnterpriseLicenseManager.ERROR_UNKNOWN
      );
      CustomizationStatus customizationStatus = new CustomizationStatus(false, "(KnoxLicenseSamsung): " + CUSTOMIZATION_OK);
      if (samsungReturnValue != EnterpriseLicenseManager.ERROR_NONE) {
        customizationStatus = new CustomizationStatus(true, "(KnoxLicenseSamsung): " + context.getString(R.string.mdm_license_samsung_error_basic));
      }

      if (KnoxLicenseSamsung.m_asynchronousResponse != null) {
        m_asynchronousResponse.onResponse(customizationStatus);
        m_asynchronousResponse = null;
      }
    }
  }

}
