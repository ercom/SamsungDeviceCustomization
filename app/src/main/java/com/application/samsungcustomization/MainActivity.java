package com.application.samsungcustomization;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.application.samsungcustomization.utils.CustomizationAsync;
import com.application.samsungcustomization.utils.CustomizationStatus;
import com.application.samsungcustomization.utils.CustomizationSync;

import java.util.LinkedList;

public class MainActivity extends AppCompatActivity {

    LinkedList<CustomizationSync> customizationList;
    TextView m_status_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Button customizeButton = (Button) findViewById(R.id.customize_button);
        m_status_text = (TextView) findViewById(R.id.customize_status);
        customizeButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        activateDeviceAdmin();
                    }
                }
        );
    }

    private void activateDeviceAdmin() {
        new AdminPermission().customize(this, new CustomizationAsync.AsynchronousResponse() {
            @Override
            public void onResponse(final CustomizationStatus customizationStatus) {
                if (!MainActivity.this.isDestroyed()) {
                    m_status_text.append(customizationStatus.m_statusMessage + "\n");
                    if (!customizationStatus.m_isError) {
                        activateElmKnoxLicense();
                    }
                }
            }
        });
    }

    private void activateElmKnoxLicense() {
        new KnoxLicenseSamsung().customize(this, new CustomizationAsync.AsynchronousResponse() {
            @Override
            public void onResponse(final CustomizationStatus customizationStatus) {
                if (!MainActivity.this.isDestroyed()) {
                    m_status_text.append(customizationStatus.m_statusMessage + "\n");
                    if (!customizationStatus.m_isError) {
                        customizeShortcuts();
                    }
                }
            }
        });
    }

    private void customizeShortcuts() {
        new KnoxShortcutCustomization().customize(this, new CustomizationAsync.AsynchronousResponse() {
            @Override
            public void onResponse(final CustomizationStatus customizationStatus) {
                if (!MainActivity.this.isDestroyed()) {
                    m_status_text.append(customizationStatus.m_statusMessage + "\n");
                    if (!customizationStatus.m_isError) {
                        startCustomization();
                    }
                }
            }
        });
    }

    private void startCustomization() {
        initializeCustomizations();
        launchCustomizations();
    }

    private void initializeCustomizations() {
        customizationList = new LinkedList();
        customizationList.add(new WallpaperCustomization());
        customizationList.add(new KnoxNativeBrowserCustomization());
    }

    private void launchCustomizations() {
        for (CustomizationSync customizationItem : customizationList) {
            CustomizationStatus customizationStatus = customizationItem.customize(this);
            m_status_text.append(customizationStatus.m_statusMessage + "\n");
            if (customizationStatus.m_isError) {
                break;
            }
        }
    }





}
