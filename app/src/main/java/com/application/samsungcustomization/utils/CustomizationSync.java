package com.application.samsungcustomization.utils;

import android.content.Context;

/**
 * Created by aci on 26/03/18.
 */

public abstract class CustomizationSync extends Customization {

    public final CustomizationStatus customize(final Context context) {
        CustomizationStatus customizationStatus = customizeSpecific(context);
        customizationStatus.m_statusMessage = "(" + getCustomizationName() + "): " + customizationStatus.m_statusMessage;
        return customizationStatus;
    }

    protected abstract CustomizationStatus customizeSpecific(final Context context);

}
