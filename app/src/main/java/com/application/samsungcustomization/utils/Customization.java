package com.application.samsungcustomization.utils;

/**
 * Created by aci on 27/03/18.
 */

public abstract class Customization {
    abstract protected String getCustomizationName();
}
