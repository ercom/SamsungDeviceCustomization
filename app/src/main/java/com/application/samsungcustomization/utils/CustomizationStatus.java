package com.application.samsungcustomization.utils;

/**
 * Created by aci on 27/03/18.
 */

public class CustomizationStatus {

    public static String CUSTOMIZATION_OK = "Customization OK";

    public boolean m_isError = false;
    public String m_statusMessage = CUSTOMIZATION_OK;

    public CustomizationStatus() {

    }

    public CustomizationStatus(boolean isError, String statusMessage) {
        m_isError = isError;
        m_statusMessage = statusMessage;
    }
}
