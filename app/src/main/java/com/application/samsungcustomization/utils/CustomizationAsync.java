package com.application.samsungcustomization.utils;

import android.content.Context;

/**
 * Created by aci on 27/03/18.
 */

public abstract class CustomizationAsync extends Customization {

    public interface AsynchronousResponse {
        void onResponse(CustomizationStatus customizationStatus);
    }

    public final void customize(final Context context, final AsynchronousResponse asynchronousResponse) {
        customizeSpecific(context, asynchronousResponse);
    }

    protected abstract void customizeSpecific(final Context context, final AsynchronousResponse asynchronousResponse);

}
