package com.application.samsungcustomization;

import android.app.Activity;
import android.app.admin.DeviceAdminReceiver;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.application.samsungcustomization.utils.CustomizationAsync;
import com.application.samsungcustomization.utils.CustomizationStatus;

import static com.application.samsungcustomization.utils.CustomizationStatus.CUSTOMIZATION_OK;

public class AdminPermission extends CustomizationAsync {

  private static AsynchronousResponse m_asynchronousResponse; /// kuldge

  @Override
  protected String getCustomizationName() {
    return "AdminPermission";
  }

  @Override
  protected void customizeSpecific(Context context, AsynchronousResponse asynchronousResponse) {
    m_asynchronousResponse = asynchronousResponse;
    context.startActivity(
            new Intent(context, AdminPermissionActivity.class)
    );
  }
  /**
   * Use to know if application is device Admin.
   * @param context the actual context
   * @return true if we are device admin
   */
  public Boolean isAdmin(Context context) {
    DevicePolicyManager devicePolicyManager;
    devicePolicyManager = context.getSystemService(DevicePolicyManager.class);
    return devicePolicyManager.isAdminActive(new ComponentName(context.getApplicationContext(), CustomizationDeviceAdmin.class));
  }

  public static class CustomizationDeviceAdmin extends DeviceAdminReceiver {
    @Override
    public void onEnabled(Context context, Intent intent) {
      if (m_asynchronousResponse != null) {
        m_asynchronousResponse.onResponse(new CustomizationStatus(false, "(AdminPermission): " + CUSTOMIZATION_OK));
        m_asynchronousResponse = null;
      }
    }
  }


  public static class AdminPermissionActivity extends Activity {
    private static final int ADMIN_ACTIVITY_ID = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
      intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN,
              new ComponentName(getApplicationContext(),
                      CustomizationDeviceAdmin.class
              ));

      startActivityForResult(intent ,ADMIN_ACTIVITY_ID);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
      if (requestCode == ADMIN_ACTIVITY_ID) {
        if (!new AdminPermission().isAdmin(this)) {
          if (m_asynchronousResponse != null) {
            m_asynchronousResponse.onResponse(new CustomizationStatus(true, "(AdminPermission): Error when activate device admin"));
            m_asynchronousResponse = null;
          }
        }
        finish();
      }
    }
  }

}
