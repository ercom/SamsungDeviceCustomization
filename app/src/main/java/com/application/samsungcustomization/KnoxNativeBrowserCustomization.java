package com.application.samsungcustomization;

import android.annotation.SuppressLint;
import android.app.enterprise.EnterpriseDeviceManager;
import android.app.enterprise.MiscPolicy;
import android.content.Context;
import android.net.Uri;
import android.util.Pair;

import com.application.samsungcustomization.utils.CustomizationStatus;
import com.application.samsungcustomization.utils.CustomizationSync;

import java.util.Arrays;
import java.util.LinkedList;


/**
 * Created by aci on 28/03/18.
 */

public class KnoxNativeBrowserCustomization extends CustomizationSync {

    private static final LinkedList<Pair<String, String>> KNOX_BOOKMARKS_LIST =
            new LinkedList<Pair<String, String>>(Arrays.asList(
                    new Pair<String, String>("http://www.google.fr", "Google"),
                    new Pair<String, String>("https://maps.google.fr", "Maps"),
                    new Pair<String, String>("https://www.yahoo.fr", "Yahoo")
            ));

    @Override
    protected String getCustomizationName() {
        return "KnoxNativeBrowser";
    }

    @Override
    protected CustomizationStatus customizeSpecific(Context context) {
        @SuppressLint("WrongConstant") EnterpriseDeviceManager edm = (EnterpriseDeviceManager)
                context.getSystemService(EnterpriseDeviceManager.ENTERPRISE_POLICY_SERVICE);
        MiscPolicy miscPolicy = edm.getMiscPolicy();
        CustomizationStatus customizationStatus = new CustomizationStatus();
        for (Pair<String, String> bookMark : KNOX_BOOKMARKS_LIST) {
            customizationStatus = customizeCreateWebBookMark(miscPolicy, bookMark.first, bookMark.second);
            if (customizationStatus.m_isError) {
                break;
            }
        }
        return customizationStatus;
    }

    private CustomizationStatus customizeCreateWebBookMark(MiscPolicy miscPolicy, String bookMarkUrl, String bookMarkName) {
        CustomizationStatus customizationStatus = new CustomizationStatus();
        miscPolicy.deleteWebBookmark(Uri.parse(bookMarkUrl), bookMarkName); // avoid accumulation
        boolean status = miscPolicy.addWebBookmarkBitmap(Uri.parse(bookMarkUrl), bookMarkName, null);
        if (!status) {
            customizationStatus.m_isError = true;
            customizationStatus.m_statusMessage = "Error when knox bookmark for url: " + bookMarkUrl;
        }
        return customizationStatus;
    }
}
